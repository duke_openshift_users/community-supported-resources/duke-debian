FROM debian

LABEL maintainer='Nate Childers <nate.childers@duke.edu>'

LABEL vendor='Duke University, Openshift Users Group' \
      architecture='x86_64' \
      summary='debian base image, updated' \
      description='Base image for Debian stable' \
      distribution-scope='private' \
      authoritative-source-url='https://gitlab.oit.duke.edu/duke_openshift_users/community-supported-resources/duke-debian' \
      git_repository_url='https://gitlab.oit.duke.edu/duke_openshift_users/community-supported-resources/duke-debian'
# TODO: Add the commit sha as a label
# the Openshift buildconfig will set this via
# LABEL io.openshift.build.commit.id
#     git_commit=${GIT_COMMIT_SHA}

LABEL version='1.0' \
      release='1'

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get upgrade -y && \
    rm -rf /var/lib/apt/lists/*
