The home of duke-debian, a [DOUG Community Supported Resource](https://duke_openshift_users.pages.oit.duke.edu/community-supported-resources/guide/)

There's a very simple Dockerfile that just updates the official debian image to current.

The OpenShift objects are documented in the file debian.yaml. There is:
  - an imagestream that imports the offical image
  - an imagestream to hold our updated image
  - a buildconfig that governs building our image from the Dockerfile